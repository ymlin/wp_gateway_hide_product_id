<?php
/**
 * @package wp-gateway-hide
 * @version 1.6
 */
/*
Plugin Name: wp-gateway-hide
Plugin URI: http://wordpress.org/plugins/
Description: wp-gateway-hide
Author: ymlin
Version: 1.0
Author URI: http://ma.tt/
*/

add_action('admin_menu', 'wp_gateway_hide');
  
function wp_gateway_hide() {
    add_submenu_page(
        'options-general.php',
        '會員訂單',
        '會員訂單',
        'manage_options',
        'upgrade',
        'mem_order_head' );
}

function gateway_head(){ ?>
  
<?php
}



function ymlin_disable_shipping_by_cats($shippings){
	global $woocommerce;
	
	//商品A(高運費)
	$target_cats_A=array(2586,2587);
	//商品B(低運費)
	$target_cats_B=array(2588,2591,2585,83);
	$gateway_A='flat_rate:2';
	$gateway_B='flat_rate:1';
	$cat_A=FALSE;
	
	$cat_B=FALSE;
	
	
	

	$cart_items=$woocommerce->cart->get_cart();
	foreach($cart_items as $item){		
			if(in_array($item['product_id'],$target_cats_A)){
				$cat_A=TRUE;
										
			}
	}
	foreach($cart_items as $item){
			if(in_array($item['product_id'],$target_cats_B)){
				$cat_B=TRUE;
	
		}
	}
	if($cat_A ){
		//wc_print_notice( __( 'A error message unset B category with high priority.', 'woocommerce' ), 'success' );
		unset($shippings[$gateway_B]);
		
	}
	else if(!$cat_A && $cat_B){
		//wc_print_notice( __( 'A error message unset A category with high priority.', 'woocommerce' ), 'error' );
		unset($shippings[$gateway_A]);
	}
	return $shippings;
}

add_filter('woocommerce_package_rates','ymlin_disable_shipping_by_cats',40);
?>